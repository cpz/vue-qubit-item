import Vue from 'vue'
import App from './App.vue'
import cachedFetch from './helpers/fetch'

Vue.use(cachedFetch); // localstorage cache

new Vue({
  el: '#app',
  render: h => h(App)  
})
