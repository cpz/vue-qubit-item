# Qubit

> Qubit item view

En este ejemplo uso Vue cli, webpack-simple template.
Para cachear requests uso localstorage.
Una mejor opcion, para una version completa de la app seria la de PWA,
usando service workers, se podria cachear la webapp entera,
faltarian tambien el ruteo y el state management entre otras cosas.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
